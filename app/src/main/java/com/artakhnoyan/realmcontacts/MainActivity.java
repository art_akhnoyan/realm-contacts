package com.artakhnoyan.realmcontacts;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;

import com.artakhnoyan.realmcontacts.callback.FragmentCallback;
import com.artakhnoyan.realmcontacts.fragment.AddContactFragment;
import com.artakhnoyan.realmcontacts.fragment.ContactListFragment;
import com.artakhnoyan.realmcontacts.fragment.ImagePreviewFragment;
import com.artakhnoyan.realmcontacts.fragment.ViewContactFragment;
import com.artakhnoyan.realmcontacts.model.Document;
import com.artakhnoyan.realmcontacts.utils.RealmDBUtils;

import java.util.List;

import io.realm.Realm;

public class MainActivity extends AppCompatActivity implements FragmentCallback {

    private Realm mRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRealm = Realm.getDefaultInstance();

        if (savedInstanceState == null) {
            ContactListFragment contactsFragment = new ContactListFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, contactsFragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    public ActionBar getActivityActionBar() {
        return getSupportActionBar();
    }

    @Override
    public Realm getRealm() {
        return mRealm;
    }

    @Override
    public void openAddContactFragment() {
        AddContactFragment contactsFragment = new AddContactFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .replace(R.id.container, contactsFragment)
                .commit();
    }

    @Override
    public void openViewContactFragment(String contactID) {
        ViewContactFragment contactsFragment = new ViewContactFragment();
        Bundle args = new Bundle();
        args.putString(RealmDBUtils.CONTACT_ID, contactID);
        contactsFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .replace(R.id.container, contactsFragment)
                .commit();
    }

    @Override
    public void openPhotoViewer(List<Document> docs, int position) {
        ImagePreviewFragment imagePreviewFragment = new ImagePreviewFragment();
        imagePreviewFragment.setDocumentsAndPosition(docs, position);
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack(null)
                .replace(R.id.container, imagePreviewFragment)
                .commit();
    }

    @Override
    public void onBack() {
        onBackPressed();
    }
}
