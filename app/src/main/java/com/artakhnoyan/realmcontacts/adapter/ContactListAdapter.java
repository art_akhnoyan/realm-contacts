package com.artakhnoyan.realmcontacts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.listener.ItemClickListener;
import com.artakhnoyan.realmcontacts.model.Contact;
import com.artakhnoyan.realmcontacts.utils.RealmDBUtils;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Artur on 7/28/2017.
 */

public class ContactListAdapter extends RecyclerView.Adapter<ContactListAdapter.ContactListViewHolder>
        implements RealmChangeListener<RealmResults<Contact>> {

    private Context mContext;
    private RealmResults<Contact> mRealmContacts;
    private List<Contact> mContacts;
    private ItemClickListener mListener;
    private Realm mRealm;

    public ContactListAdapter(Context context, Realm realm) {
        mContext = context;
        mRealm = realm;
        mRealmContacts = RealmDBUtils.retrieveAllContacts(realm);
        mRealmContacts.addChangeListener(this);
    }

    public void setItemClickListener(ItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public ContactListAdapter.ContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact_list, parent, false);

        return new ContactListViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(ContactListAdapter.ContactListViewHolder holder, int position) {
        Contact contact = mContacts.get(position);
        holder.textViewName.setText(contact.getName() + " " + contact.getSurname());
        holder.textViewPhone.setText(mContext.getString(R.string.label_phone, contact.getPhone()));
        holder.textViewEmail.setText(mContext.getString(R.string.label_email, contact.getEmail()));
    }

    @Override
    public int getItemCount() {
        if (mContacts == null) {
        return 0;
    }
        return mContacts.size();
    }

    public Contact getItem(int position) {
        return mContacts.get(position);
    }

    @Override
    public void onChange(RealmResults<Contact> contacts) {
        mContacts = mRealm.copyFromRealm(contacts);
        notifyDataSetChanged();
    }

    static class ContactListViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewPhone;
        TextView textViewEmail;

        ContactListViewHolder(View itemView, final ItemClickListener listener) {
            super(itemView);
            textViewName = (TextView) itemView.findViewById(R.id.tv_contact_name);
            textViewPhone = (TextView) itemView.findViewById(R.id.tv_contact_phone);
            textViewEmail = (TextView) itemView.findViewById(R.id.tv_contact_email);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });
        }
    }
}
