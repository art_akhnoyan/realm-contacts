package com.artakhnoyan.realmcontacts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.listener.ItemClickListener;
import com.artakhnoyan.realmcontacts.model.Document;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Artur on 7/29/2017.
 * Adapter for {@link Document} that created for {@link com.artakhnoyan.realmcontacts.model.Contact}
 */

public class DocumentAdapter extends RecyclerView.Adapter<DocumentAdapter.DocumentViewHolder> {

    private static final int ITEM_TYPE_ATTACH = 25;
    private List<Document> mDocuments;
    private ItemClickListener mListener;
    private Context mContext;

    public DocumentAdapter(Context context, List<Document> documents, ItemClickListener listener) {
        mDocuments = documents;
        mListener = listener;
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return ITEM_TYPE_ATTACH;
        }
        return super.getItemViewType(position);
    }

    @Override
    public DocumentAdapter.DocumentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == ITEM_TYPE_ATTACH) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_attach_doc, parent, false);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_document, parent, false);
        }

        return new DocumentViewHolder(view, mListener);
    }

    @Override
    public void onBindViewHolder(DocumentAdapter.DocumentViewHolder holder, int position) {
        if (position != 0) {
            Document document = mDocuments.get(position - 1);
            Glide.with(mContext).load(document.getDocPath())
                    .into(holder.documentImageView);
        }
    }

    @Override
    public int getItemCount() {
        if (mDocuments == null) {
            return 1;
        }
        return mDocuments.size() + 1;
    }

    static class DocumentViewHolder extends RecyclerView.ViewHolder {

        ImageView documentImageView;

        DocumentViewHolder(View itemView, final ItemClickListener listener) {
            super(itemView);
            documentImageView = (ImageView) itemView.findViewById(R.id.iv_document);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(getAdapterPosition());
                }
            });

        }
    }
}
