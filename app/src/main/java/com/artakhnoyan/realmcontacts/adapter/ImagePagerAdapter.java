package com.artakhnoyan.realmcontacts.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.model.Document;
import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Artur on 7/29/2017.
 * Adapter created for viewing attached {@link Document} fullscreen
 */

public class ImagePagerAdapter extends PagerAdapter {

    private List<Document> mImages;

    public ImagePagerAdapter(List<Document> images) {
        mImages = images;
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == (FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
        Document document = mImages.get(position);
        Glide.with(container.getContext())
                .load(document.getDocPath())
                .into(imageView);
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
