package com.artakhnoyan.realmcontacts.callback;

import android.support.v7.app.ActionBar;

import com.artakhnoyan.realmcontacts.model.Document;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Artur on 7/28/2017.
 */

/**
 * {@link FragmentCallback} interface created for communication between fragments adn their activity
 */
public interface FragmentCallback {
    ActionBar getActivityActionBar();

    Realm getRealm();

    void openAddContactFragment();

    void openViewContactFragment(String contactID);

    void openPhotoViewer(List<Document> docs, int position);

    void onBack();
}
