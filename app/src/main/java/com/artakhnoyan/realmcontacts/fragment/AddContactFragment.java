package com.artakhnoyan.realmcontacts.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.adapter.DocumentAdapter;
import com.artakhnoyan.realmcontacts.model.Contact;
import com.artakhnoyan.realmcontacts.utils.RealmDBUtils;
import com.artakhnoyan.realmcontacts.utils.Utils;

import java.util.UUID;

/**
 * A simple {@link ContactFragment} subclass.
 */
public class AddContactFragment extends ContactFragment {

    public AddContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = new DocumentAdapter(getContext().getApplicationContext(), mDocuments, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContact = new Contact();
        mDocumentsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.btn_done:
                String name = mEditTextName.getText().toString();
                String surname = mEditTextSurname.getText().toString();
                String email = mEditTextEmail.getText().toString();
                String phone = mEditTextPhone.getText().toString();
                if (Utils.isInputsFilled(getContext(), name, surname, email, phone)
                        && Utils.checkForEmail(getContext(), email)
                        && Utils.checkForPhoneNumber(getContext(), phone)) {
                    mContact.setContactID(UUID.randomUUID().toString());
                    mContact.setName(name);
                    mContact.setSurname(surname);
                    mContact.setEmail(email);
                    mContact.setPhone(phone);
                    mContact.getDocs().addAll(mDocuments);
                    RealmDBUtils.createOrUpdateContact(mCallback.getRealm(), mContact);
                    // back to home screen
                    mCallback.onBack();
                    return false;
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
