package com.artakhnoyan.realmcontacts.fragment;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.artakhnoyan.realmcontacts.callback.FragmentCallback;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    private static final String TAG = BaseFragment.class.getSimpleName();

    protected FragmentCallback mCallback;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (FragmentCallback) context;
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach: ", e);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }
}
