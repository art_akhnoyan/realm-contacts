package com.artakhnoyan.realmcontacts.fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.adapter.DocumentAdapter;
import com.artakhnoyan.realmcontacts.listener.ItemClickListener;
import com.artakhnoyan.realmcontacts.model.Contact;
import com.artakhnoyan.realmcontacts.model.Document;
import com.artakhnoyan.realmcontacts.utils.Utils;
import com.scanlibrary.ScanConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class ContactFragment extends BaseFragment implements ItemClickListener {

    private static final int REQUEST_CODE = 99;
    private static final int REQUEST_STORAGE = 25;

    protected EditText mEditTextName;
    protected EditText mEditTextSurname;
    protected EditText mEditTextEmail;
    protected EditText mEditTextPhone;
    protected RecyclerView mDocumentsRecyclerView;
    protected DocumentAdapter mAdapter;
    protected Contact mContact;
    protected List<Document> mDocuments;
    protected boolean isDocsLoaded = false;


    public ContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        mDocuments = new ArrayList<>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem doneItem = menu.findItem(R.id.btn_done);
        doneItem.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                //back to home screen
                mCallback.onBack();
                break;
        }
        return false;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_contact, container, false);
        mEditTextName = (EditText) rootView.findViewById(R.id.et_name);
        mEditTextSurname = (EditText) rootView.findViewById(R.id.et_surname);
        mEditTextEmail = (EditText) rootView.findViewById(R.id.et_email);
        mEditTextPhone = (EditText) rootView.findViewById(R.id.et_phone);
        mDocumentsRecyclerView = (RecyclerView) rootView.findViewById(R.id.rv_documents);
        mDocumentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        ActionBar actionBar = mCallback.getActivityActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            final Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            if (uri != null) {
                mDocuments.add(new Document(Utils.getPathFromUri(getContext(), uri)));
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        String permission = permissions.length > 0 ? permissions[0] : null;
        if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils.openScanActivity(this, REQUEST_CODE);
            }
        }
    }

    @Override
    public void onItemClick(int position) {
        if (position == 0) {
            if (!Utils.isStoragePermissionGranted(getContext(), this, REQUEST_STORAGE)) {
            } else {
                Utils.openScanActivity(this, REQUEST_CODE);
            }
        } else {
            //open photo
            mCallback.openPhotoViewer(mDocuments, position -1);
        }
    }
}
