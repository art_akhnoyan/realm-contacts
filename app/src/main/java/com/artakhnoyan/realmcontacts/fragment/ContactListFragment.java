package com.artakhnoyan.realmcontacts.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.adapter.ContactListAdapter;
import com.artakhnoyan.realmcontacts.listener.ItemClickListener;
import com.artakhnoyan.realmcontacts.utils.RealmDBUtils;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class ContactListFragment extends BaseFragment implements ItemClickListener {

    private RecyclerView mContactsRecyclerView;
    private View mFabAddContact;
    private ContactListAdapter mContactListAdapter;

    public ContactListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mContactListAdapter = new ContactListAdapter(getContext(),
                mCallback.getRealm());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        mFabAddContact = rootView.findViewById(R.id.fab_add_contact);
        mFabAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open add contact fragment
                mCallback.openAddContactFragment();
            }
        });
        mContactsRecyclerView = (RecyclerView) rootView.findViewById(R.id.contacts_list);
        mContactsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));
        mContactListAdapter.setItemClickListener(this);
        mContactsRecyclerView.setAdapter(mContactListAdapter);

        return rootView;
    }

    @Override
    public void onItemClick(int position) {
        Log.d(TAG, "onItemClick: " + position);
        mCallback.openViewContactFragment(mContactListAdapter.getItem(position).getContactID());
    }
}
