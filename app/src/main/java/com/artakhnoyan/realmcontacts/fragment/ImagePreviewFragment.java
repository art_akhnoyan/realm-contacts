package com.artakhnoyan.realmcontacts.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.adapter.ImagePagerAdapter;
import com.artakhnoyan.realmcontacts.model.Document;

import java.util.List;

/**
 * A simple {@link BaseFragment} subclass.
 */
public class ImagePreviewFragment extends BaseFragment {

    private List<Document> mDocuments;
    private int mPosition;
    private ImagePagerAdapter mAdapter;

    public ImagePreviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        mAdapter = new ImagePagerAdapter(mDocuments);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                mCallback.onBack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_image_preview, container, false);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.vp_images);
        pager.setAdapter(mAdapter);
        pager.setCurrentItem(mPosition);
        ActionBar actionBar = mCallback.getActivityActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        return rootView;
    }

    public void setDocumentsAndPosition(List<Document> documents, int position) {
        mDocuments = documents;
        mPosition = position;
    }

}
