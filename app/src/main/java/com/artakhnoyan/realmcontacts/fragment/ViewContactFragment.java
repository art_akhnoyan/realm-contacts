package com.artakhnoyan.realmcontacts.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import android.view.View;

import com.artakhnoyan.realmcontacts.R;
import com.artakhnoyan.realmcontacts.adapter.DocumentAdapter;
import com.artakhnoyan.realmcontacts.utils.RealmDBUtils;
import com.artakhnoyan.realmcontacts.utils.Utils;

/**
 * A simple {@link ContactFragment} subclass.
 */
public class ViewContactFragment extends ContactFragment {

    public ViewContactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		mAdapter = new DocumentAdapter(getContext().getApplicationContext(), mDocuments, this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String contactId = getArguments().getString(RealmDBUtils.CONTACT_ID);
        mContact = RealmDBUtils.getContactById(mCallback.getRealm(), contactId);
        mEditTextName.setText(mContact.getName());
        mEditTextSurname.setText(mContact.getSurname());
        mEditTextEmail.setText(mContact.getEmail());
        mEditTextPhone.setText(mContact.getPhone());
        if (!isDocsLoaded) {
            mDocuments.addAll(mContact.getDocs());
            isDocsLoaded = true;
        }
        mDocumentsRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case R.id.btn_done:
                final String name = mEditTextName.getText().toString();
                final String surname = mEditTextSurname.getText().toString();
                final String email = mEditTextEmail.getText().toString();
                final String phone = mEditTextPhone.getText().toString();
                if (Utils.isInputsFilled(getContext(), name, surname, email, phone)
                        && Utils.checkForEmail(getContext(), email)
                        && Utils.checkForPhoneNumber(getContext(), phone)) {
                    mContact.setName(name);
                    mContact.setSurname(surname);
                    mContact.setEmail(email);
                    mContact.setPhone(phone);
                    if (!mContact.getDocs().isEmpty()) {
                        mContact.getDocs().clear();
                    }
                    mContact.getDocs().addAll(mDocuments);
                    RealmDBUtils.createOrUpdateContact(mCallback.getRealm(), mContact);
                    // back to home screen
                    mCallback.onBack();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
