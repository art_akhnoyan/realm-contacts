package com.artakhnoyan.realmcontacts.listener;

/**
 * Created by Artur on 7/29/2017.
 * for adapter Item click ( {@link com.artakhnoyan.realmcontacts.adapter.ContactListAdapter},
 * {@link com.artakhnoyan.realmcontacts.adapter.DocumentAdapter}
 */

public interface ItemClickListener {
    void onItemClick(int position);
}
