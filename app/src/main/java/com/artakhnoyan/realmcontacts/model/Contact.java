package com.artakhnoyan.realmcontacts.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Artur on 7/28/2017.
 */

public class Contact extends RealmObject {

    @PrimaryKey
    private String contactID;
    private String name;
    private String surname;
    private String email;
    private String phone;
    private RealmList<Document> docs;

    public Contact() {
        docs = new RealmList<>();
    }

    public String getContactID() {
        return contactID;
    }

    public void setContactID(String contactID) {
        this.contactID = contactID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public RealmList<Document> getDocs() {
        return docs;
    }
}
