package com.artakhnoyan.realmcontacts.model;

import io.realm.RealmObject;

/**
 * Created by Artur on 7/28/2017.
 */

public class Document extends RealmObject {

    private String docPath;

    public Document() {

    }

    public Document(String docPath) {
        this.docPath = docPath;
    }

    public String getDocPath() {
        return docPath;
    }

    public void setDocPath(String docPath) {
        this.docPath = docPath;
    }
}
