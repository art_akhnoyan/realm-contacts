package com.artakhnoyan.realmcontacts.utils;

import com.artakhnoyan.realmcontacts.model.Contact;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Artur on 7/28/2017.
 */

public class RealmDBUtils {

    public static final String CONTACT_ID = "contactID";

    public static void createOrUpdateContact(Realm realm, final Contact contact) {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(contact);
            }
        });
    }

    public static Contact getContactById(Realm realm, String id) {
        Contact contact = realm.where(Contact.class)
                .equalTo(CONTACT_ID, id)
                .findFirst();
        return realm.copyFromRealm(contact);
    }

    public static RealmResults<Contact> retrieveAllContacts(Realm realm) {
        return realm.where(Contact.class).findAllAsync();
    }
}
