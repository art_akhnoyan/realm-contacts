package com.artakhnoyan.realmcontacts.utils;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Toast;

import com.artakhnoyan.realmcontacts.R;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

/**
 * Created by Artur on 7/29/2017.
 */

public class Utils {

    public static boolean checkForEmail(Context context, String email) {
        if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return true;
        } else {
            Toast.makeText(context, context.getString(R.string.incorrect_email)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static boolean checkForPhoneNumber(Context context, String phone) {
        if (Patterns.PHONE.matcher(phone).matches()) {
            return true;
        } else {
            Toast.makeText(context, context.getString(R.string.incorrect_phone)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static boolean isInputsFilled(Context context, String name,
                                         String surname, String email, String phone) {
        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(surname) && !TextUtils.isEmpty(email)
                && !TextUtils.isEmpty(phone)) {
            return true;
        } else {
            Toast.makeText(context, context.getString(R.string.please_fill_up_form)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public static void openScanActivity(Fragment fragment, int requestCode) {
        int preference = ScanConstants.OPEN_MEDIA;
        Intent intent = new Intent(fragment.getContext(), ScanActivity.class);
        intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
        fragment.startActivityForResult(intent, requestCode);
    }

    public static String getPathFromUri(Context context, Uri uri) {
        String result = null;

        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(index);
            cursor.close();
        }
        return result;
    }
    public static boolean isStoragePermissionGranted(Context context, Fragment fragment, int requestCode) {
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            fragment.requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    requestCode);
            return false;
        } else {
            return true;
        }
    }
}
